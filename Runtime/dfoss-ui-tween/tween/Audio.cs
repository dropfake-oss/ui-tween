namespace UI.Tweening
{
	public interface IAudioForTweens
	{
		void PlaySoundForTween(string soundName);
	}
}
