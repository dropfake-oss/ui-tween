using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI.Tweening
{
	public static partial class DropTween
	{

		public static Tweener TranslateX(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			Translate elemTranslate = nElement.style.translate.value;

			float newX = nFrom;
			return DOTween.To(() => newX, x => newX = x, nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnUpdate(() =>
			{
				elemTranslate.x = new Length(newX);
				nElement.style.translate = elemTranslate;
			}).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener TranslateX(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			Translate elemTranslate = nElement.style.translate.value;

			float newX = elemTranslate.x.value;
			return DOTween.To(() => newX, x => newX = x, nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnUpdate(() =>
			{
				elemTranslate.x = new Length(newX);
				nElement.style.translate = elemTranslate;
			}).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener TranslateY(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			Translate elemTranslate = nElement.style.translate.value;

			float newY = nFrom;
			return DOTween.To(() => newY, x => newY = x, nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnUpdate(() =>
			{
				elemTranslate.y = new Length(newY);
				nElement.style.translate = elemTranslate;
			}).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener TranslateY(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			Translate elemTranslate = nElement.style.translate.value;

			float newY = elemTranslate.y.value;
			return DOTween.To(() => newY, x => newY = x, nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnUpdate(() =>
			{
				elemTranslate.y = new Length(newY);
				nElement.style.translate = elemTranslate;
			}).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener TranslateXY(VisualElement nElement, Vector2 nTo, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			Translate elemTranslate = nElement.style.translate.value;

			Vector2 newPos = new Vector2(elemTranslate.x.value, elemTranslate.y.value);
			return DOTween.To(() => newPos, x => newPos = x, nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnUpdate(() =>
			{
				elemTranslate.x = new Length(newPos.x);
				elemTranslate.y = new Length(newPos.y);
				nElement.style.translate = elemTranslate;
			}).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
	}
}
