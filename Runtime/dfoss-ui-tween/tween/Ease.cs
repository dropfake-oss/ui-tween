using DG.Tweening;

namespace UI.Tweening
{
	public static partial class DropTween
	{
		public class EaseConfiguration
		{
			public const float DefaultSpeed = 1.0f;
			public float Speed;

			public const Ease DefaultEase = Ease.OutCirc;
			public Ease Ease;

			public const float DefaultDelay = 0.0f;
			public float Delay;

			public EaseConfiguration()
			{
				this.Speed = DefaultSpeed;
				this.Ease = DefaultEase;
				this.Delay = DefaultDelay;
			}

			public EaseConfiguration(EaseConfiguration ease)
			{
				this.Speed = ease.Speed;
				this.Ease = ease.Ease;
				this.Delay = ease.Delay;
			}
		}
	}
}
