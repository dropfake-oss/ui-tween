using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI.Tweening
{
	public static partial class DropTween
	{
		public class ScaleConfiguration
		{
			public const float DefaultDuration = 0.75f;
			public float Duration = DefaultDuration;

			public const float DefaultDelay = 0.0f;
			public float Delay = DefaultDelay;

			public const Ease DefaultEase = Ease.OutCirc;
			public Ease Ease = DefaultEase;
		}

		public static Tweener ScaleIn(VisualElement nElement, ScaleConfiguration? configuration = null)
		{
			var duration = (configuration != null) ? configuration.Duration : ScaleConfiguration.DefaultDuration;
			var delay = (configuration != null) ? configuration.Delay : ScaleConfiguration.DefaultDelay;
			var ease = (configuration != null) ? configuration.Ease : ScaleConfiguration.DefaultEase;

			return ScaleIn(nElement, duration, ease, delay);
		}

		public static Tweener ScaleIn(VisualElement nElement, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0)
		{
			return DOTween.To(() => new Vector3(0, 0, 1), x => nElement.transform.scale = x, new Vector3(1, 1, 1), nSpeed).SetEase(nEase).SetDelay(nDelay);
		}

		public static Tweener ScaleOut(VisualElement nElement, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.transform.scale, x => nElement.transform.scale = x, new Vector3(0, 0, 1), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener ScaleX(VisualElement nElement, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var scale = nElement.transform.scale;
			var scaleY = scale.y;
			var scaleZ = scale.z;

			return DOTween.To(() => nElement.transform.scale, x => nElement.transform.scale = x, new Vector3(nTo, scaleY, scaleZ), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener ScaleX(VisualElement nElement, float nFrom = 0, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var scale = nElement.transform.scale;
			var scaleY = scale.y;
			var scaleZ = scale.z;

			return DOTween.To(() => new Vector3(nFrom, scaleY, scaleZ), x => nElement.transform.scale = x, new Vector3(nTo, scaleY, scaleZ), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		// TODO: ugh. scaleX and sclaeY are owrking differently right now. need to fix that (x starts at 0. need a fromto or something)
		public static Tweener ScaleY(VisualElement nElement, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var scale = nElement.transform.scale;
			var scaleX = scale.x;
			var scaleZ = scale.z;

			return DOTween.To(() => nElement.transform.scale, x => nElement.transform.scale = x, new Vector3(scaleX, nTo, scaleZ), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener ScaleY(VisualElement nElement, float nFrom = 1, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var scale = nElement.transform.scale;
			var scaleX = scale.x;
			var scaleZ = scale.z;

			return DOTween.To(() => new Vector3(scaleX, nFrom, scaleZ), x => nElement.transform.scale = x, new Vector3(scaleX, nTo, scaleZ), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}


		public static Tweener Scale(VisualElement nElement, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var scale = nElement.transform.scale;
			var scaleZ = scale.z;

			return DOTween.To(() => nElement.transform.scale, x => nElement.transform.scale = x, new Vector3(nTo, nTo, scaleZ), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener Scale(VisualElement nElement, float nSpeed = 0.75f, float nFrom = 1, float nTo = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var scale = nElement.transform.scale;
			var scaleX = scale.x;
			var scaleY = scale.y;
			var scaleZ = scale.z;

			return DOTween.To(() => new Vector3(nFrom, nFrom, scaleZ), x => nElement.transform.scale = x, new Vector3(nTo, nTo, scaleZ), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener Scale(IStyle style, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			return DOTween.To(() => style.scale.value.value.x, x => style.scale = new Scale(new Vector3(x, x)), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener Scale(IStyle style, float nFrom = 1, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			return DOTween.To(() => nFrom, x => style.scale = new Scale(new Vector3(x, x)), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
	}
}
