using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI.Tweening
{
	public static partial class DropTween
	{
		private const float FlickerSpeed = 0.05f;

		private const float SlideX = 60f;
		public const float Duration = 0.3f;
		//private AudioManager Audio = GameObject.FindObjectOfType<AudioManager>();
		static DropTween()
		{
			DOTween.SetTweensCapacity(500, 550);
		}

		public static IAudioForTweens? AudioForTweens = null;

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// for making sequences outside of DropTween
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Sequence DropTweenSequence()
		{
			return DOTween.Sequence();
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// FOR CALLING FUNCTIONS (at specific times, not just at the end of a tween)
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tween Func(Action? nCallback, float nDelay = 0)
		{
			var seq = DOTween.Sequence();

			seq.SetDelay(nDelay);
			seq.SetAutoKill(false);
			seq.OnComplete(() => nCallback());

			return seq;
		}
#region SCREEN ANIMS
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// SCREEN ANIMS : TODO break out in to own file
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tween ScreenIn(VisualElement nScreen, Action? nCallback = null)
		{
			var seq = DOTween.Sequence();
			if (nCallback != null) seq.OnComplete(() => nCallback());

			seq.Insert(0, DropTween.Scale(nScreen, 1, 0, Ease.Linear));
			seq.Insert(0, DropTween.Top(nScreen, 200, 0, 0.45f, Ease.OutBack));
			seq.Insert(0, DropTween.Fade(nScreen, 0, 1, 0.25f, Ease.OutQuad));

			return seq;
		}
		public static Tween ScreenInBack(VisualElement nScreen, Action? nCallback = null)
		{
			var seq = DOTween.Sequence();
			if (nCallback != null) seq.OnComplete(() => nCallback());

			seq.Insert(0, DropTween.Scale(nScreen, 0.45f, 0.45f, 1, Ease.OutBack));
			seq.Insert(0, DropTween.Fade(nScreen, 0, 1, 0.25f, Ease.OutQuad));

			return seq;
		}
		public static Tween ScreenOut(VisualElement nScreen, Action? nCallback = null)
		{
			var seq = DOTween.Sequence();
			if (nCallback != null) seq.OnComplete(() => nCallback());

			seq.Insert(0, DropTween.Scale(nScreen, 0.45f, 0.35f, Ease.InBack));
			seq.Insert(0.2f, DropTween.Fade(nScreen, 0, 0.15f, Ease.InQuad));

			return seq;
		}
		public static Tween ScreenOutBack(VisualElement nScreen, Action? nCallback = null)
		{
			var seq = DOTween.Sequence();
			if (nCallback != null) seq.OnComplete(() => nCallback());

			seq.Insert(0, DropTween.Top(nScreen, 200, 0.35f, Ease.InBack));
			seq.Insert(0.2f, DropTween.Fade(nScreen, 0, 0.15f, Ease.InQuad));

			return seq;
		}
#endregion
#region TYPOGRAPHY
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// TEXT ANIMS : TODO break out in to own file
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tweener LetterSpacing(Label nLabel, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nLabel.style.letterSpacing.value.value, x => nLabel.style.letterSpacing = x, nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener LetterSpacing(Label nLabel, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nLabel.style.letterSpacing = x, nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tween WordInByCharacter(Label nLabel, string nString = "", float nSpeed = 1, int nSteps = 10, Action? nCallback = null)
		{
			var seq = DOTween.Sequence();
			if (nCallback != null) seq.OnComplete(() => nCallback());

			// what is the string we are using? do we use the string set on the label, or pass in a string?
			// if we pass in a string, use that, otherwise use what the label has
			if (nString == "")
			{
				nString = nLabel.text;
			}
			int lengthOfString = nString.Length;
			int i;
			int j;
			int k;
			int numCharactersToShow = 0;
			float stepDelay = nSpeed / nSteps;

			nLabel.text = "";

			for (i = 1; i < nSteps + 1; i++)
			{
				numCharactersToShow = (int)Mathf.Round(lengthOfString * ((float)i / (float)nSteps));

				string binaryStringThingy = "";
				for (j = 0; j < lengthOfString; j++)
				{
					if (j < numCharactersToShow)
					{
						binaryStringThingy = binaryStringThingy + "1";
					}
					else
					{
						binaryStringThingy = binaryStringThingy + "0";
					}
				}

				// shuffle the string (binary to pick which letters to show/hide)
				binaryStringThingy = ShuffleString(binaryStringThingy);

				// okay, now let's rebuild the string, hiding the selected letters
				string htmlString = "";
				for (k = 0; k < lengthOfString; k++)
				{
					string binaryChar = (string)"" + binaryStringThingy[k];
					string curChar = (string)"" + nString[k];

					if (binaryChar == "0")
					{
						htmlString = htmlString + "<color=#00000000>" + curChar + "</color>";
					}
					else
					{
						htmlString = htmlString + curChar;
					}
				}
				seq.Insert(i * stepDelay, DropTween.Func(() => UpdateShuffledLabel(nLabel, htmlString)));
			}
			seq.Insert(nSpeed, DropTween.Func(() => UpdateShuffledLabel(nLabel, nString)));
			return seq;
		}


		public static Tween TypeInText(Label nLabel, string nString = "", Action? nCallback = null, float nStepDelay = 0.01f)
		{
			var seq = DOTween.Sequence();
			if (nCallback != null) seq.OnComplete(() => nCallback());

			if (nString == "")
			{
				nString = nLabel.text;
			}

			int i;
			int nSteps = nString.Length;
			float stepDelay = nStepDelay;

			for (i = 1; i < nSteps + 1; i++)
			{
				string currentText = nString.Substring(0, i);
				seq.Insert(i * stepDelay, DropTween.Func(() => UpdateShuffledLabel(nLabel, currentText)));
			}
			seq.Insert(i * stepDelay, DropTween.Func(() => UpdateShuffledLabel(nLabel, nString)));
			return seq;
		}

		private static void UpdateShuffledLabel(Label nLabel, string nString)
		{
			nLabel.enableRichText = true;
			nLabel.text = nString;
		}

		public static string ShuffleString(this string str)
		{
			char[] array = str.ToCharArray();
			System.Random rng = new System.Random();
			int n = array.Length;
			while (n > 1)
			{
				n--;
				int k = rng.Next(n + 1);
				var value = array[k];
				array[k] = array[n];
				array[n] = value;
			}
			return new string(array);
		}
#endregion

#region FLICKER
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// UI ANIMS WITH AUDIO
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tween FlickerInWithAudio(VisualElement nElement, int nNumFlickers = 0, float nDelay = 0, string nSoundName = "", float nSoundVolume = -1, Action? nCallback = null)
		{
			// number of flickers needs to be odd. so if it isn't, add one to make sure we end on a full alpha

			int numFlickers = nNumFlickers % 2 == 0 ? nNumFlickers + 1 : nNumFlickers;
			var style = nElement.style;
			var seq = DOTween.Sequence();
			float delay = nDelay;
			int i = 0;
			int alpha;
			Action? callback = null;
			bool playSound;

			if (nSoundName != "")
			{
				AudioForTweens?.PlaySoundForTween(nSoundName);
			}

			for (i = 0; i < numFlickers; i++)
			{
				alpha = i % 2 == 0 ? 1 : 0;
				callback = i == numFlickers - 1 ? nCallback : null;
				playSound = i % 2 == 0;
				seq.Insert(delay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), alpha, 0).OnStart(() => { /*Debug.Log(GameObject.FindObjectOfType<AudioManager>());*/ /*if(playSound) GameObject.FindObjectOfType<AudioManager>().Play("wave_b");*/ /* play some audio in here */ }).OnComplete(() =>
					{
						callback?.Invoke();
					}));

				delay += FlickerSpeed;
			}

			/*if(GameObject.FindObjectOfType<AudioManager>())
			{
				GameObject.FindObjectOfType<AudioManager>().Play("wave_a");
			}*/

			/*
			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0));

			seq.Insert(FlickerSpeed + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 0, 0));

			seq.Insert((FlickerSpeed * 2) + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0));
			*/
			return seq;
		}

		public static Tween FlickerInWithAudio(VisualElement nElement, float nDelay = 0, string nSoundName = "", Action? nCallback = null)
		{
			var style = nElement.style;
			var seq = DOTween.Sequence();

			if (nSoundName != "")
			{
				nElement.schedule.Execute(() =>
				{
					AudioForTweens?.PlaySoundForTween(nSoundName);
				}).StartingIn((long)(nDelay * 1000));
			}

			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0));

			seq.Insert(FlickerSpeed + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 0, 0));

			seq.Insert((FlickerSpeed * 2) + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0).OnComplete(() =>
					{
						nCallback?.Invoke();
					}));

			return seq;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// COLOUR FLICKERING
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/*public static Tween FlickerColour(VisualElement nElement, Color? nColour = null, int nNumFlickers = 2, float nDelay = 0, Action nCallback = null)
		{
			// number of flickers needs to be odd. so if it isn't, add one to make sure we end on a full alpha

			if(nColour == null)
			{
				nColour = Color.red;
			}
			int numFlickers = nNumFlickers % 2 == 0 ? nNumFlickers + 1 : nNumFlickers;
			var style = nElement.style;
			var seq = DOTween.Sequence();
			float delay = nDelay;
			int i = 0;
			int alpha;

			Action? callback = null;

			for (i = 0; i < numFlickers; i++)
			{
				alpha = i % 2 == 0 ? 1 : 0;
				c = i % 2 == 0 ? nColour : Color.white;
				callback = i == numFlickers - 1 ? nCallback : null;
				seq.Insert(delay, DOTween.To(() => nElement.resolvedStyle.color,
					x => style.color = c, alpha, 0).OnStart(() => {  }).OnComplete(() =>
					{
						callback?.Invoke();
					}));

				delay += FlickerSpeed;
			}

			return seq;
		}
		*/
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// FLICKER
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tween FlickerIn(VisualElement nElement, float nDelay = 0, Action? nCallback = null)
		{
			var style = nElement.style;
			var seq = DOTween.Sequence();

			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0));

			seq.Insert(FlickerSpeed + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 0, 0));

			seq.Insert((FlickerSpeed * 2) + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0).OnComplete(() =>
					{
						nCallback?.Invoke();
					}));

			return seq;
		}

		public static Tween FlickerOut(VisualElement nElement, float nDelay = 0, float nFlickerAlpha = 0.25f, Action? nCallback = null)
		{
			var style = nElement.style;
			var seq = DOTween.Sequence();

			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 0, 0));

			seq.Insert(FlickerSpeed + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), nFlickerAlpha, 0));

			seq.Insert((FlickerSpeed * 2) + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 0, 0).OnComplete(() =>
					{
						nCallback?.Invoke();
					}));

			return seq;
		}

		public static Tween FlickerInOut(VisualElement nElement, float nDelay = 0, Action? nCallback = null)
		{
			var style = nElement.style;
			var seq = DOTween.Sequence();

			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0));

			seq.Insert(FlickerSpeed + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 0, 0));

			seq.Insert((FlickerSpeed * 2) + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 1, 0));

			seq.Insert((FlickerSpeed * 3) + nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), 0, 0).OnComplete(() =>
					{
						nCallback?.Invoke();
					}));

			return seq;
		}

		public static Tween Flicker(VisualElement nElement, int nNumFlickers = 0, float nTime = 0, float nDelay = 0, Action? nCallback = null, bool nForceEndOnFullOpactiy = true)
		{
			// number of flickers needs to be odd. so if it isn't, add one to make sure we end on a full alpha

			float nStep = nTime / nNumFlickers;
			/*if(nTime == 0)
			{
				nTime = FlickerSpeed;
			}*/

			int numFlickers;

			if (nForceEndOnFullOpactiy)
			{
				numFlickers = nNumFlickers % 2 == 0 ? nNumFlickers + 1 : nNumFlickers;
			}
			else
			{
				numFlickers = nNumFlickers;
			}

			var style = nElement.style;
			var seq = DOTween.Sequence();
			float delay = nDelay;
			int i = 0;
			int alpha;
			Action? callback = null;

			for (i = 0; i < numFlickers; i++)
			{
				alpha = i % 2 == 0 ? 1 : 0;
				callback = i == numFlickers - 1 ? nCallback : null;
				seq.Insert(delay, DOTween.To(() => nElement.resolvedStyle.opacity,
					x => style.opacity = new StyleFloat(x), alpha, 0).OnComplete(() =>
					{
						callback?.Invoke();
					}));

				delay += nStep;
			}

			return seq;
		}

#endregion
#region SIZE
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// SIZE (these are a bit funky. might be best to split them in to width and height tweens?)
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tween Size(VisualElement nElement, Vector2 nSize, float nSpeed = 10.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var style = nElement.style;
			var seq = DOTween.Sequence();

			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.width,
					x => style.width = new StyleLength(x), nSize.x, nSpeed).SetEase(nEase));
			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.height,
					x => style.height = new StyleLength(x), nSize.y, nSpeed).SetEase(nEase).OnComplete(() =>
					{
						nCallback?.Invoke();
					}));

			return seq;
		}

		public static Tween SizeFromTo(VisualElement nElement, Vector2 nFrom, Vector2 nTo, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var seq = DOTween.Sequence();
			var style = nElement.style;

			seq.Insert(nDelay, DOTween.To(() => nFrom.x,
					x => style.width = new StyleLength(x), nTo.x, nSpeed).SetEase(nEase));
			seq.Insert(nDelay, DOTween.To(() => nFrom.y,
					x => style.height = new StyleLength(x), nTo.y, nSpeed).SetEase(nEase).OnComplete(() =>
					{
						nCallback?.Invoke();
					}));

			return seq;
		}

		public static Tweener Width(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nElement.style.width.value.value, x => nElement.style.width = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nElement.style.width.value.value, x => nElement.style.width = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}
		public static Tweener Width(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nFrom, x => nElement.style.width = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nFrom, x => nElement.style.width = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}

		public static Tweener Height(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nElement.style.height.value.value, x => nElement.style.height = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nElement.style.height.value.value, x => nElement.style.height = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}
		public static Tweener Height(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nFrom, x => nElement.style.height = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nFrom, x => nElement.style.height = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}

		}

#endregion
#region ROTATION
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// ROTATE
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tweener Rotate(VisualElement nElement, float nRotateAmount = -360, float nSpeed = 4, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsLooping = false)
		{
			return DOTween.To(() => nElement.worldTransform.rotation.eulerAngles,
				x => nElement.transform.rotation = Quaternion.Euler(x),
				new Vector3(0, 0, nRotateAmount), nSpeed).SetEase(nEase).SetLoops(nIsLooping == true ? -1 : 0).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
		}

		public static Tweener Rotation(VisualElement nElement, float nFrom, float nTo, float nSpeed, Ease nEase = Ease.Linear, float nDelay = 0, Action? nCallback = null)
		{
			nElement.transform.rotation = Quaternion.Euler(0, 0, nFrom);
			Quaternion fromRotation = Quaternion.Euler(0, 0, nFrom);
			Quaternion toRotation = Quaternion.Euler(0, 0, nTo);
			return DOTween.To(() => 0f, t =>
			{
				nElement.transform.rotation = Quaternion.Slerp(fromRotation, toRotation, t);
			}, 1f, nSpeed).SetDelay(nDelay).SetEase(nEase).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

#endregion
#region TRANSLATE		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// POSITIONS
		//////////////////////////////////////////////////////////////////////////////////////////////////////

		/*
		public static Tweener Translate(VisualElement nElement, Vector2? nTo = null, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			if (nTo == null) nTo = new Vector2(0, 0);
			var curT = new Vector3(nElement.resolvedStyle.translate.x, nElement.resolvedStyle.translate.y, nElement.resolvedStyle.translate.z);

			return DOTween.To(() => curT, x => nElement.style.translate = new StyleTranslate(), new Vector3(nTo.Value.x, nTo.Value.y, 0), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener Translate(VisualElement nElement, Vector2? nFrom = null, Vector2? nTo = null, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			if (nFrom == null) nFrom = new Vector2(0, 0);
			if (nTo == null) nTo = new Vector2(0, 0);

			//var curT = new Vector3(nFrom.Value.x, nFrom.Value.y, nElement.resolvedStyle.translate.z);
			//var curT = new StyleTranslate(nFrom.Value.x, nFrom.Value.y, nElement.resolvedStyle.translate.z);

			return DOTween.To(() => nElement.resolvedStyle.translate, x => nElement.style.translate = x, new Vector3(nTo.Value.x, nTo.Value.y, 0), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		*/
#endregion
#region MARGINS AND PADDING
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// MARGINS
		//////////////////////////////////////////////////////////////////////////////////////////////////////

		public static Tweener MarginTop(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.style.marginTop.value.value, x => nElement.style.marginTop = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener MarginTop(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.marginTop = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener MarginRight(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.style.marginRight.value.value, x => nElement.style.marginRight = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener MarginRight(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.marginRight = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener MarginBottom(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.style.marginBottom.value.value, x => nElement.style.marginBottom = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener MarginBottom(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.marginBottom = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener MarginLeft(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.style.marginLeft.value.value, x => nElement.style.marginLeft = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener MarginLeft(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.marginLeft = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// PADDING
		//////////////////////////////////////////////////////////////////////////////////////////////////////

		public static Tweener PaddingLeft(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.style.paddingLeft.value.value, x => nElement.style.paddingLeft = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener PaddingLeft(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.paddingLeft = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener PaddingRight(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.style.paddingRight.value.value, x => nElement.style.paddingRight = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener PaddingRight(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.paddingRight = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
#endregion
#region HAPTICS
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// HAPTICS
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/*
		public static Tweener Haptic(HapticType type = HapticType.Medium, float duration = 0.1f, float delay = 0f, Action callback = null)
		{
			return DOTween.Sequence()
				.AppendCallback(() =>
				{
					
					
					#if UNITY_IOS
						switch (type)
						{
							case HapticType.Light:
								UnityEngine.iOS.Haptics.GenerateImpact(UnityEngine.iOS.Haptics.ImpactFeedbackStyle.Light);
								break;
							case HapticType.Medium:
								UnityEngine.iOS.Haptics.GenerateImpact(UnityEngine.iOS.Haptics.ImpactFeedbackStyle.Medium);
								break;
							case HapticType.Heavy:
								UnityEngine.iOS.Haptics.GenerateImpact(UnityEngine.iOS.Haptics.ImpactFeedbackStyle.Heavy);
								break;
							case HapticType.Selection:
								UnityEngine.iOS.Haptics.GenerateSelection();
								break;
							case HapticType.Success:
								UnityEngine.iOS.Haptics.GenerateNotification(UnityEngine.iOS.Haptics.NotificationFeedbackType.Success);
								break;
							case HapticType.Warning:
								UnityEngine.iOS.Haptics.GenerateNotification(UnityEngine.iOS.Haptics.NotificationFeedbackType.Warning);
								break;
							case HapticType.Error:
								UnityEngine.iOS.Haptics.GenerateNotification(UnityEngine.iOS.Haptics.NotificationFeedbackType.Error);
								break;
							case HapticType.Custom:
								if (UnityEngine.iOS.Device.systemVersion.StartsWith("13.") || 
									int.Parse(UnityEngine.iOS.Device.systemVersion.Split('.')[0]) > 13)
								{
									var pattern = new[] { duration / 3f, duration / 3f, duration / 3f };
									var intensities = new[] { 0.5f, 0.8f, 1.0f };
									UnityEngine.iOS.Haptics.GenerateHaptics(pattern, intensities);
								}
								else
								{
									UnityEngine.iOS.Haptics.GenerateImpact(UnityEngine.iOS.Haptics.ImpactFeedbackStyle.Medium);
								}
								break;
						}
					#elif UNITY_ANDROID
						if (Application.platform == RuntimePlatform.Android)
						{
							using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
							using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
							using (AndroidJavaObject vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator"))
							{
								if (vibrator.Call<bool>("hasVibrator"))
								{
									long durationMs = (long)(duration * 1000);
									if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
									{
										int androidIntensity = GetAndroidIntensity(type);
										
										switch (type)
										{
											case HapticType.Success:
											case HapticType.Warning:
											case HapticType.Error:
												// Create a simple pattern for notifications
												long[] timings = { 0, durationMs, 100, durationMs };
												int[] amplitudes = { 0, androidIntensity, 0, androidIntensity };
												vibrator.Call("vibrate", VibrationEffect.CreateWaveform(timings, amplitudes, -1));
												break;
											default:
												vibrator.Call("vibrate", VibrationEffect.CreateOneShot(durationMs, androidIntensity));
												break;
										}
									}
									else
									{
										vibrator.Call("vibrate", durationMs);
									}
								}
							}
						}
					#endif
					
				})
				.SetDelay(delay)
				.OnComplete(() => callback?.Invoke());
		}

		private static int GetAndroidIntensity(HapticType type)
		{
			switch (type)
			{
				case HapticType.Light:
					return 64;  // 25% of max
				case HapticType.Medium:
					return 128; // 50% of max
				case HapticType.Heavy:
					return 255; // 100% of max
				case HapticType.Selection:
					return 32;  // 12.5% of max
				case HapticType.Success:
					return 192; // 75% of max
				case HapticType.Warning:
					return 160; // 62.5% of max
				case HapticType.Error:
					return 224; // 87.5% of max
				case HapticType.Custom:
					return 128; // 50% of max (default to medium)
				default:
					return 128; // 50% of max (default to medium)
			}
		}
		*/
#endregion
#region MISC
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// MISC TWEENS
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static Tweener FontSize(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nElement.style.fontSize.value.value, x => nElement.style.fontSize = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener FontSize(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.fontSize = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener Shake(VisualElement nElement, Action? nCallback = null, float shakeDuration = 0.3f, Vector3? shakeStrengths = null, int shakeVibrato = 20, float shakeRandomness = 0, bool doFade = true, float delay = 0)
		{
			if (shakeStrengths == null)
			{
				shakeStrengths = new Vector3(10, 10, 1);
			}

			return DOTween.Shake(() => new Vector3(0, 0, 0), x => nElement.transform.position = x, shakeDuration, (Vector3)shakeStrengths, shakeVibrato, shakeRandomness, doFade).SetDelay(delay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener ShakeRotation(VisualElement nElement, Action? nCallback = null, float shakeDuration = 0.3f)
		{
			//float shakeDuration = 0.3f;
			float shakeStregth = 4f;
			int shakeVibrato = 2;
			float shakeRandomness = 10;

			return DOTween.Shake(() => nElement.worldTransform.rotation.eulerAngles, x => nElement.transform.rotation = Quaternion.Euler(x), shakeDuration, shakeStregth, shakeVibrato, shakeRandomness, true).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener IntUpdate(int nValue, int nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action<int>? nUpdateCallback = null, Action? nOnCompleteCallback = null)
		{
			return DOTween.To(() => nValue, x => nValue = x, nTo, nSpeed).SetDelay(nDelay).SetEase(nEase)
			.OnUpdate(() =>
			{
				nUpdateCallback?.Invoke(nValue);
			}).OnComplete(() =>
			{
				nOnCompleteCallback?.Invoke();
			});
		}

		public static Tweener IntUpdate(int nValue, int nFrom = 0, int nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action<int>? nUpdateCallback = null, Action? nOnCompleteCallback = null)
		{
			return DOTween.To(() => nFrom, x => nValue = x, nTo, nSpeed).SetDelay(nDelay).SetEase(nEase)
			.OnUpdate(() =>
			{
				nUpdateCallback?.Invoke(nValue);
			}).OnComplete(() =>
			{
				nOnCompleteCallback?.Invoke();
			});
		}

		public static Tweener FloatUpdate(float nValue, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action<float>? nUpdateCallback = null, Action? nOnCompleteCallback = null)
		{
			return DOTween.To(() => nValue, x => nValue = x, nTo, nSpeed).SetDelay(nDelay).SetEase(nEase)
			.OnUpdate(() =>
			{
				nUpdateCallback?.Invoke(nValue);
			}).OnComplete(() =>
			{
				nOnCompleteCallback?.Invoke();
			});
		}
#endregion
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// KILL CANCEL TWEENS
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		public static void CancelAnim(Sequence anim)
		{
			if (anim != null)
			{
				anim.Kill();
			}
		}

		public static void Kill(VisualElement nTarget, bool nComplete = false)
		{
			DOTween.Kill(nTarget, nComplete);
		}
	}
}
