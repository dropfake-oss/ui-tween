using DG.Tweening;
using System;
using UnityEngine.UIElements;

namespace UI.Tweening
{
	public static partial class DropTween
	{
		public class FadeConfiguration : EaseConfiguration
		{
			public float From = 1.0f;
			public float To = 1.0f;

			public new const float DefaultSpeed = 0.75f;

			public FadeConfiguration()
			{
				this.From = 1.0f;
				this.To = 1.0f;

				Speed = DefaultSpeed;
			}

			public FadeConfiguration(FadeConfiguration fade)
				:
			base(fade)
			{
				this.From = fade.From;
				this.To = fade.To;
			}
		}

		public class FadeOutConfiguration : FadeConfiguration
		{
			public const float DefaultFrom = 1.0f;
			public const float DefaultTo = 0.0f;

			public FadeOutConfiguration()
			{
				this.From = DefaultFrom;
				this.To = DefaultTo;
			}

			public FadeOutConfiguration(FadeConfiguration fade)
				:
			base(fade)
			{
			}
		}

		public static Tween FadeOut(VisualElement nElement, FadeConfiguration? configuration, Action? nCallback = null)
		{
			var from = (configuration != null) ? configuration.From : FadeOutConfiguration.DefaultFrom;
			var to = (configuration != null) ? configuration.To : FadeOutConfiguration.DefaultTo;
			var speed = (configuration != null) ? configuration.Speed : FadeOutConfiguration.DefaultSpeed;
			var delay = (configuration != null) ? configuration.Delay : FadeOutConfiguration.DefaultDelay;
			var ease = (configuration != null) ? configuration.Ease : FadeOutConfiguration.DefaultEase;

			return Fade(nElement, from, to, speed, ease, delay, nCallback);
		}

		public class FadeInConfiguration : FadeConfiguration
		{
			public const float DefaultFrom = 0.0f;
			public const float DefaultTo = 1.0f;

			public FadeInConfiguration()
			{
				this.From = DefaultFrom;
				this.To = DefaultTo;
			}

			public FadeInConfiguration(FadeConfiguration fade)
				:
			base(fade)
			{
			}
		}

		public static Tween FadeIn(VisualElement nElement, FadeConfiguration configuration, Action? nCallback = null)
		{
			var from = (configuration != null) ? configuration.From : FadeInConfiguration.DefaultFrom;
			var to = (configuration != null) ? configuration.To : FadeInConfiguration.DefaultTo;
			var speed = (configuration != null) ? configuration.Speed : FadeInConfiguration.DefaultSpeed;
			var delay = (configuration != null) ? configuration.Delay : FadeInConfiguration.DefaultDelay;
			var ease = (configuration != null) ? configuration.Ease : FadeInConfiguration.DefaultEase;

			return Fade(nElement, from, to, speed, ease, delay, nCallback);
		}

		public static Tween Fade(VisualElement nElement, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			var style = nElement.style;

			// using a sequence here because tween wasn't working for pip animation. hmmm
			var seq = DOTween.Sequence();

			seq.Insert(nDelay, DOTween.To(() => nElement.resolvedStyle.opacity,
				x => style.opacity = new StyleFloat(x), nTo, nSpeed).SetEase(nEase).OnComplete(() =>
				{
					nCallback?.Invoke();
				}));

			return seq;
		}

		public static Tween Fade(VisualElement nElement, float nFrom = 1, float nTo = 1, float nSpeed = 0.75f, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.opacity = new StyleFloat(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
	}
}
