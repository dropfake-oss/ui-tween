using System.Collections;
using System.Collections.Generic;
using UI.Core;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI.Tweening
{
	public class ContentParallaxer : VisualElementWithSubscriptions
	{
		public class ContentParallaxItem
		{
			public VisualElement? Item;
			public float? Depth;
			public Vector3? StartingPosition;
		}

		public new class UxmlFactory : UxmlFactory<ContentParallaxer, UxmlTraits> { }
		public new class UxmlTraits : VisualElement.UxmlTraits { }

		private List<ContentParallaxItem> ParallaxItems = new();

		public int MinDepth = 1;
		public int DepthMultiplier = 2;
		private float GravityXStart = 0.0f;
		private float GravityYStart = 0.0f;
		private float GravityXCurrent = 0.0f;
		private float GravityYCurrent = 0.0f;

		private bool CanParallax = false;

		public Label? DebugLabel;

		// Start is called before the first frame update
		public override void Awake()
		{
			if (!IsInitialized) { Initialize(); }

			GravityXStart = Input.gyro.gravity.x;
			GravityYStart = Input.gyro.gravity.y;
		}


		private void Initialize()
		{
			//Debug.Log("TeamVSTeam - Initialize()");

			IsInitialized = true;
			if (SystemInfo.supportsGyroscope)
			{
				Input.gyro.enabled = true;
				CanParallax = true;


			}
		}

		// Update is called once per frame
		public void Update()
		{
			if (!CanParallax)
			{
				//Debug.Log("ContentParallaxer - Update() CanParallax:" + CanParallax);
				return;
			}
			//Debug.Log("ContentParallaxer - Update() GravityXCurrent:" + GravityXCurrent + " GravityYCurrent:" + GravityYCurrent+ " ParallaxItems:"+ ParallaxItems.Count);

			GravityXCurrent = GravityXStart - Input.gyro.gravity.x;
			GravityYCurrent = GravityYStart - Input.gyro.gravity.y;

			if (DebugLabel != null)
			{
				//DebugLabel.text = "x:" + GravityXCurrent + " y:" + GravityYCurrent;
			}

			VisualElement ve;
			float depth;
			float layerPos;
			foreach (ContentParallaxItem nItem in ParallaxItems)
			{
				if (nItem.Item != null && nItem.Depth != null && nItem.StartingPosition != null)
				{
					ve = nItem.Item;
					depth = (float)nItem.Depth;
					layerPos = (depth * DepthMultiplier) + MinDepth;
					ve.transform.position = new Vector3(nItem.StartingPosition.Value.x - (GravityXCurrent * layerPos), nItem.StartingPosition.Value.y + (GravityYCurrent * layerPos), 0);
				}
			}
		}

		public void AddItem(ContentParallaxItem nItem)
		{
			//Debug.Log("ContentParallaxer - AddItem() ");

			if (nItem.Item != null)
			{
				if (nItem.StartingPosition == null)
				{
					nItem.StartingPosition = nItem.Item.transform.position;
				}
				ParallaxItems.Add(nItem);
			}
			//Update();
		}

		public void Reset()
		{
			ParallaxItems = new();
			CanParallax = false;
		}

		public void Continue()
		{
			if (SystemInfo.supportsGyroscope)
			{
				GravityXStart = Input.gyro.gravity.x;
				GravityYStart = Input.gyro.gravity.y;
				Input.gyro.enabled = true;
				CanParallax = true;
			}
		}
	}
}
