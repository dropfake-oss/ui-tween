using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI.Tweening
{
	public static partial class DropTween
	{
		public class PositionConfiguration : EaseConfiguration
		{
			public float From;

			public float To;

			public const bool DefaultIsPercentage = false;
			public bool IsPercentage;

			public PositionConfiguration()
			{
				From = 0.0f;
				To = 0.0f;
				IsPercentage = DefaultIsPercentage;
			}

			public PositionConfiguration(PositionConfiguration position)
				:
			base(position)
			{
				From = position.From;
				To = position.To;
				IsPercentage = position.IsPercentage;
			}
		}

		public static Tweener PosY(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			float xPos = nElement.transform.position.x;
			float zPos = nElement.transform.position.z;

			return DOTween.To(() => nElement.transform.position, newPos => nElement.transform.position = newPos, new Vector3(xPos, nTo, zPos), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener PosY(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			float xPos = nElement.transform.position.x;
			float zPos = nElement.transform.position.z;

			return DOTween.To(() => new Vector3(xPos, nFrom, zPos), newPos => nElement.transform.position = newPos, new Vector3(xPos, nTo, zPos), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener PosX(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			float yPos = nElement.transform.position.y;
			float zPos = nElement.transform.position.z;

			return DOTween.To(() => nElement.transform.position, newPos => nElement.transform.position = newPos, new Vector3(nTo, yPos, zPos), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
		public static Tweener PosX(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			float yPos = nElement.transform.position.y;
			float zPos = nElement.transform.position.z;

			return DOTween.To(() => new Vector3(nFrom, yPos, zPos), newPos => nElement.transform.position = newPos, new Vector3(nTo, yPos, zPos), nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}

		public static Tweener PosXY(VisualElement nElement, Vector2 nTo, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			Vector3 startPos = nElement.transform.position;

			return DOTween.To(() => nElement.transform.position, newPos => nElement.transform.position = newPos, new Vector3(nTo.x, nTo.y, startPos.z), nSpeed)
				.SetEase(nEase)
				.SetDelay(nDelay)
				.OnComplete(() =>
				{
					nCallback?.Invoke();
				});
		}

		public static Tweener PosXY(VisualElement nElement, Vector2 nFrom, Vector2 nTo, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action nCallback = null)
		{
			Vector3 startPos = nElement.transform.position;

			return DOTween.To(() => new Vector3(nFrom.x, nFrom.y, startPos.z), newPos => nElement.transform.position = newPos, new Vector3(nTo.x, nTo.y, startPos.z), nSpeed)
				.SetEase(nEase)
				.SetDelay(nDelay)
				.OnComplete(() =>
				{
					nCallback?.Invoke();
				});
		}

		public class LeftConfiguration : PositionConfiguration
		{
			public const float DefaultFrom = 0.0f;
			public const float DefaultTo = 0.0f;

			public LeftConfiguration()
			{
				this.From = DefaultFrom;
				this.To = DefaultTo;
			}

			public LeftConfiguration(PositionConfiguration position)
				:
			base(position)
			{
			}
		}

		public static Tweener Left(VisualElement nElement, PositionConfiguration? configuration, Action? nCallback = null)
		{
			var from = (configuration != null) ? configuration.From : LeftConfiguration.DefaultFrom;
			var to = (configuration != null) ? configuration.To : LeftConfiguration.DefaultTo;
			var speed = (configuration != null) ? configuration.Speed : LeftConfiguration.DefaultSpeed;
			var delay = (configuration != null) ? configuration.Delay : LeftConfiguration.DefaultDelay;
			var ease = (configuration != null) ? configuration.Ease : LeftConfiguration.DefaultEase;
			var isPercentage = (configuration != null) ? configuration.IsPercentage : LeftConfiguration.DefaultIsPercentage;

			return Left(nElement, from, to, speed, ease, delay, nCallback, isPercentage);
		}
		public static Tweener Left(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nElement.style.left.value.value, x => nElement.style.left = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nElement.style.left.value.value, x => nElement.style.left = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}
		public static Tweener Left(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nFrom, x => nElement.style.left = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nFrom, x => nElement.style.left = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}

		public static Tweener Right(VisualElement nElement, float nAmount = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nElement.style.right.value.value, x => nElement.style.right = Length.Percent(x), nAmount, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nElement.style.right.value.value, x => nElement.style.right = new StyleLength(x), nAmount, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}
		public static Tweener Right(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nFrom, x => nElement.style.right = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nFrom, x => nElement.style.right = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}

		public class TopToConfiguration : PositionConfiguration
		{
			public const float DefaultTo = 0.0f;

			public TopToConfiguration()
			{
				this.To = DefaultTo;
			}

			public TopToConfiguration(PositionConfiguration position)
				:
			base(position)
			{
			}
		}

		public static Tweener TopTo(VisualElement nElement, PositionConfiguration? configuration, Action? nCallback = null)
		{
			var to = (configuration != null) ? configuration.To : TopToConfiguration.DefaultTo;
			var speed = (configuration != null) ? configuration.Speed : TopToConfiguration.DefaultSpeed;
			var delay = (configuration != null) ? configuration.Delay : TopToConfiguration.DefaultDelay;
			var ease = (configuration != null) ? configuration.Ease : TopToConfiguration.DefaultEase;
			var isPercentage = (configuration != null) ? configuration.IsPercentage : TopToConfiguration.DefaultIsPercentage;

			return Top(nElement, to, speed, ease, delay, nCallback, isPercentage);
		}
		public static Tweener Top(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nElement.style.top.value.value, x => nElement.style.top = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nElement.style.top.value.value, x => nElement.style.top = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}
		public static Tweener Top(VisualElement nElement, float nFrom = -100, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nFrom, x => nElement.style.top = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nFrom, x => nElement.style.top = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}

		public static Tweener Bottom(VisualElement nElement, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null, bool nIsPercentage = false)
		{
			if (nIsPercentage)
			{
				return DOTween.To(() => nElement.style.bottom.value.value, x => nElement.style.bottom = Length.Percent(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
			else
			{
				return DOTween.To(() => nElement.style.bottom.value.value, x => nElement.style.bottom = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
				{
					nCallback?.Invoke();
				});
			}
		}
		public static Tweener Bottom(VisualElement nElement, float nFrom = 0, float nTo = 0, float nSpeed = 1, Ease nEase = Ease.OutCirc, float nDelay = 0, Action? nCallback = null)
		{
			return DOTween.To(() => nFrom, x => nElement.style.bottom = new StyleLength(x), nTo, nSpeed).SetEase(nEase).SetDelay(nDelay).OnComplete(() =>
			{
				nCallback?.Invoke();
			});
		}
	}
}
